package org.studia;

import java.io.*;
import java.util.List;

public class DataWriter {
    private final static String DEFAULT_OUTPUT_FILENAME = "output.data";

    public static boolean writeData(List<Object[]> outputData, String filename) {
        if(filename==null || filename.isEmpty()){
            filename = DEFAULT_OUTPUT_FILENAME;
        }
        try(Writer writer = new BufferedWriter(new OutputStreamWriter (new FileOutputStream(filename)))) {
            for(Object[] object : outputData){
                for(int i=0; i<object.length; i++){
                    if (object[i] instanceof String) {
                        writer.write((String) object[i]);
                    } else if(object[i] instanceof Double){
                        writer.write(((Double)object[i]).toString());
                    }
                    if(i < object.length-1) {
                        writer.write(",");
                    }
                }
                writer.write("\n");
            }
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
