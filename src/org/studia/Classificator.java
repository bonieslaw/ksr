package org.studia;

import java.util.*;

public class Classificator {

    public static final String EUKLIDEAN = "Euklidean";
    public static final String MANHATTAN = "Manhattan";
    public static final String CHEBYSHEV = "Chebyshev";

    int classCellNumber;

    MetricSpace metric;
    NeighbourComparator comparator = new NeighbourComparator();

    private class Neighbour implements Comparable<Neighbour> {
        public Neighbour(double distance) {
            this.distance = distance;
        }

        public String className;
        public double distance;

        @Override
        public int compareTo(Neighbour o) {
            if (this.distance > o.distance) {
                return 1;
            } else if (this.distance < o.distance) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    private class NeighbourComparator implements Comparator<Neighbour> {

        @Override
        public int compare(Neighbour o1, Neighbour o2) {
            return o1.compareTo(o2);
        }
    }

    public Classificator(String distanceCountingMethod, int classCellNumber) {
        this.classCellNumber = classCellNumber;
        setMetric(distanceCountingMethod, classCellNumber);
    }

    public void setMetric(String distanceCountingMethod, int classCellNumber) {
        switch (distanceCountingMethod) {
            default:
            case EUKLIDEAN:
                metric = new EuklideanMetric(classCellNumber);
                break;
            case MANHATTAN:
                metric = new ManhattanMetric(classCellNumber);
                break;
            case CHEBYSHEV:
                metric = new ChebyshevMetric(classCellNumber);
                break;
        }
    }

    public List<Object[]> prepareTestData(List<Object[]> testData) {
        List<Object[]> newList = new ArrayList<Object[]>();
        for (Object[] object : testData) {
            Object[] newObject = new Object[object.length + 1];
            for (int i = 0; i < newObject.length; i++) {
                if (i >= classCellNumber) {
                    if (i == classCellNumber) {
                        newObject[i] = " ";
                        continue;
                    }
                    newObject[i] = object[i - 1];
                }
                try {
                    newObject[i] = object[i];
                } catch (ArrayIndexOutOfBoundsException e) {

                }
            }
            newList.add(newObject);
        }
        return newList;
    }

    private void clearList(List<Neighbour> list) {
        for (int it = 0; it < list.size(); it++) {
            list.set(it, new Neighbour(Double.POSITIVE_INFINITY));
        }
        list.sort(comparator);
    }

    private String getMostCommon(List<Neighbour> list) {
        Set<String> checkedClasses = new HashSet<String>();
        String winner = "";
        int count = 0;
        int tmpCount;
        for (Neighbour n : list) {
            if (!checkedClasses.contains(n.className)) {
                checkedClasses.add(n.className);
                tmpCount = 0;
                for (Neighbour n2 : list) {
                    if (n.className == n2.className) {
                        tmpCount++;
                    }
                }
                if (tmpCount > count) {
                    count = tmpCount;
                    winner = n.className;
                }
            }
        }
        return winner;
    }

    public List<Object[]> kNN(List<Object[]> testData, List<Object[]> trainingData, int k) {
        testData = prepareTestData(testData);
        List<Neighbour> neighbours = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            neighbours.add(new Neighbour(Double.POSITIVE_INFINITY));
        }
        double distance;
        for (Object[] testedObject : testData) {
            for (Object[] trainingObject : trainingData) {
                distance = metric.distance(testedObject, trainingObject);
                if (distance < neighbours.get(neighbours.size() - 1).distance) {
                    neighbours.get(neighbours.size() - 1).distance = distance;
                    neighbours.get(neighbours.size() - 1).className = (String) trainingObject[classCellNumber];
                }
                neighbours.sort(comparator);
//                for (int n = neighbours.size() - 1; n >= 0; n--) {
//                    if (distance < neighbours.get(n).distance) {
//                        neighbours.get(n).distance = distance;
//                        neighbours.get(n).className = (String) trainingObject[classCellNumber];
//                        neighbours.sort(comparator);
//                        break;
//                    }
//                }
            }
            testedObject[classCellNumber] = getMostCommon(neighbours);
            clearList(neighbours);
        }
        return testData;
    }
}
