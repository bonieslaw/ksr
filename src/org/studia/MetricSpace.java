package org.studia;

public abstract class MetricSpace {

    protected final int classCellNumber;

    public MetricSpace(int classCellNumber) {
        this.classCellNumber = classCellNumber;
    }

    public double distance(Object[] Obj1, Object[] Obj2) {
        if(Obj1.length != Obj2.length) {
            return -1.0D;
        }
        return countDistance(Obj1, Obj2);
    }
    protected abstract double countDistance(Object[] Obj1, Object[] Obj2);
}
