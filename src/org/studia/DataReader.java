package org.studia;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataReader {

    private final static String DEFAULT_IRIS_DATA_FILENAME = "iris.data";

    public static List<Object[]> readData(String filename) {
        if (filename == null || filename.isEmpty()) {
            filename = DEFAULT_IRIS_DATA_FILENAME;
        }
        List<Object[]> objectsList = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            if (reader.ready()) {
                objectsList = new ArrayList<Object[]>();
                String line = reader.readLine();
                while (line != null) {
                    objectsList.add(line.split(","));
                    line = reader.readLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return objectsList;
    }

    public static List<Object[]> convertObjectsList(List<Object[]> inList) {
        return null;
    }

    public int findClassFieldIndex(List<Object[]> inList, int expectedClassesCount) {
        List<Set<Object>> list = new ArrayList<Set<Object>>();
        for (Object field : inList.get(0)) {
            list.add(new HashSet<Object>());
        }
        return 0;
    }
}
