package org.studia;

import java.util.List;

public class ClassificationService {

    public static void performClassificationTests(String testData, String trainingData, String outputDataPrefix, int classFieldIndex, int k) {
        List<Object[]> testObjects = DataReader.readData(testData);
        List<Object[]> trainingObjects = DataReader.readData(trainingData);
        List<Object[]> result;

        Classificator classificator = new Classificator(Classificator.EUKLIDEAN, classFieldIndex);
        result = classificator.kNN(testObjects, trainingObjects, k);
        DataWriter.writeData(result, outputDataPrefix + "_K" + k + "_EUKLIDEAN.data");

        classificator.setMetric(Classificator.MANHATTAN, classFieldIndex);
        result = classificator.kNN(testObjects, trainingObjects, k);
        DataWriter.writeData(result, outputDataPrefix + "_K" + k + "_MANHATTAN.data");

        classificator.setMetric(Classificator.CHEBYSHEV, classFieldIndex);
        result = classificator.kNN(testObjects, trainingObjects, k);
        DataWriter.writeData(result, outputDataPrefix + "_K" + k + "_CHEBYSHEV.data");
    }
}
