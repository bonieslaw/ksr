package org.studia;

public class EuklideanMetric extends MetricSpace {

    public EuklideanMetric(int classCellNumber) {
        super(classCellNumber);
    }

    @Override
    protected double countDistance(Object[] Obj1, Object[] Obj2) {
        double result = 0.0D;
        for (int i = 0; i < Obj1.length; i++) {
            if (i == classCellNumber) {
                continue;
            }
            result += Math.pow((Double.valueOf((String) Obj1[i]) - Double.valueOf((String) Obj2[i])), 2);
        }

        return Math.sqrt(result);
    }
}
