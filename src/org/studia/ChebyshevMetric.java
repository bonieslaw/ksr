package org.studia;

public class ChebyshevMetric extends MetricSpace {

    public ChebyshevMetric(int classCellNumber) {
        super(classCellNumber);
    }

    @Override
    protected double countDistance(Object[] Obj1, Object[] Obj2) {
        double result = 0.0D;
        for (int i = 0; i < Obj1.length; i++) {
            if (i == classCellNumber) {
                continue;
            }
            double distance = Math.abs(Double.valueOf((String) Obj1[i]) - Double.valueOf((String) Obj2[i]));
            result = result < distance ? distance : result;
        }

        return result;
    }
}
